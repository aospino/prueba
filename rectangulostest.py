import unittest
from rectangulos1 import area
from rectangulos1 import perimetro

class RectanguloTestCase(unittest.TestCase):
    def test_area(self):
        self.assertEqual(area(5, 4), 20)
    
    def test_perimetro(self):
        self.assertEqual(perimetro(5, 4), 18)

unittest.main()
